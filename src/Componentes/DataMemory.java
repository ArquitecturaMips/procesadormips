/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Componentes;
import Componentes.General;
import Componentes.ControlSignals;
/**
 *
 * @author Holman
 */
public class DataMemory {
    private int[] memDataAddrs;
    public int pActData;
    private final int staticLength = General.dataLastAddr - General.dataBaseAddr;
    
    public DataMemory(){
        this.memDataAddrs = new int[this.staticLength];
        this.pActData     = 0;
    }
    
    public int getOutput(int data,int addr){
        // Si la señal de control LMem está a 1, coger el valor de la dirección addr y retornarla //
        if(ControlSignals.LMem==1){ System.out.println("Readed from memory : " + this.getWord(addr)); return this.getWord(addr); }
        // Si no, si EMem está a 1, guardar los datos en la dirección siguiente
        else if(ControlSignals.EMem==1){ this.addWord(data); System.out.println("Writted in memory : " + memDataAddrs[this.pActData-4]); }
        return 0x00;
    }
    
    public void addWord(int data){ this.memDataAddrs[this.pActData] = data; this.pActData += 4; }
 
    public void resetMemory(){ 
        for(int i=0,act=this.memDataAddrs[i];i<this.staticLength && act!=0;this.memDataAddrs[i++]=0,act=this.memDataAddrs[i]);
        this.pActData    = 0;
    }
    
    public int getWord(int addr){ return this.memDataAddrs[addr-General.dataBaseAddr]; }
    public int getHalf(int addr){ return (short)(this.getWord(addr) & 0x0000FFFF); }
    public int getByte(int addr){ return (byte)(this.getWord(addr) & 0x000000FF); }
    
    /**public static void main(String args[]){ 
        DataMemory im = new DataMemory();
        // Control signal de escritura a 1 //
        ControlSignals.LMem = 0;
        ControlSignals.EMem = 1;
        System.out.println(im.getOutput(1337,0x00010000));
        System.out.println(im.getOutput(1338,0x00010004));
        // Control signal de lectura a 1 //
        ControlSignals.LMem = 1;
        ControlSignals.EMem = 0;
        System.out.println(im.getOutput(999,0x00010000));
        System.out.println(im.getOutput(999,0x00010004));
    }**/
    
}
